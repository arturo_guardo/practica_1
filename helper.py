from flask import jsonify

from times import today


def whatcode(code, data = None):

    if code == 200:
        return jsonify({"code": 200,
                        "data": data,
                        "status": "ok",
                        "message": "Orders Displayed",
                        "time": today()})
    elif code == 400:
        return jsonify({"code": 400,
                        "status_order": "error",
                        "message": "Bad Request",
                        "time": today()})
    elif code == 500:
        return jsonify({"code": 500,
                        "status_order": "error",
                        "message": "Update Error",
                        "time": today()})
    else:
        return jsonify({'Message': 'Products CRUD',
                        'For Create New Product': '/createfood',
                        'For Create Order': '/createorder',
                        'For Read All Orders': '/findallorders',
                        'For Update Order': '/updateorder'})
