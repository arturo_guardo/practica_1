from times import today

def id_validate(data):

    if data.isdigit():
        return True
    return False

def status_order_validate(status_order):
    if status_order.isprintable():
        if status_order.islower():
            status_order = status_order.upper()

        if status_order == "EN PREPARACIÓN" or status_order == "EN PREPARACION":
            return [status_order, today()]
        elif status_order == "EN REPARTO":
            return [status_order, today()]
        elif status_order == "ENTREGADO":
            return [status_order, today()]
        else:
            return False
    else:
        return False
    
def create_order_validate(input_order):
    for data in input_order:
        if not data.isprintable():
            return False
    return input_order