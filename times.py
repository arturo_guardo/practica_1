from datetime import datetime as date

def today():
    return date.now().strftime('%d/%m/%Y %H:%M:%S')