from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

from helper import whatcode
from times import today
from validations import id_validate, status_order_validate, create_order_validate

from re import split


app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:Red5g123@localhost:3306/db_restaurant'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Products_Table(db.Model):

    __tablename__ = 'products'

    product_id = db.Column(db.Integer,primary_key=True)
    product_name = db.Column(db.String(100))
    product_price = db.Column(db.Integer)

    #Constructor cada vez que se instancia la clase
    #Al recibir asignar los datos
    def __init__(self, product_name, product_price):

        self.product_name= product_name
        self.product_price= product_price
    #Modelo de Datos completado

class Orders_Table(db.Model):

    __tablename__= 'orders'

    order_id = db.Column(db.Integer,primary_key=True)
    client_name = db.Column(db.String(100))
    order_date = db.Column(db.String(100))
    products_name = db.Column(db.String(100))
    no_product = db.Column(db.Integer)
    total_count = db.Column(db.Integer)
    client_addrs = db.Column(db.String(100))
    client_city = db.Column(db.String(100))
    client_phone = db.Column(db.String(100))
    status_order = db.Column(db.String(100))
    check_preparation = db.Column(db.String(100))
    check_on_route = db.Column(db.String(100))
    check_delivered = db.Column(db.String(100))

    #Constructor cada vez que se instancia la clase
    #Al recibir asignar los datos
    columns = [client_name, order_date, products_name,
               no_product, total_count, client_addrs,
               client_city, client_phone, status_order,
               check_preparation, check_on_route, check_delivered]

    def __init__(self, columns):

        self.client_name = columns[0]
        self.order_date = columns[1]
        self.products_name = columns[2]
        self.no_product = columns[3]
        self.total_count = columns[4]
        self.client_addrs = columns[5]
        self.client_city = columns[6]
        self.client_phone = columns[7]
        self.status_order = columns[8]
        self.check_preparation = columns[9]
        self.check_on_route = columns[10]
        self.check_delivered = columns[11]
    #Modelo de Datos completado


db.create_all()

class Products_Table_Schema(ma.Schema):
    class Meta:
        fields = ('product_id ','product_name','product_price')

class Orders_Table_Schema(ma.Schema):
    class Meta:
        fields = ('order_id ', 'client_name', 'order_date',
                  'products_name', 'no_product', 'total_count',
                  'client_addrs', 'client_city', 'client_phone',
                  'status_order', 'check_preparation', 'check_on_route',
                  'check_delivered')

product_table_schema = Products_Table_Schema()
products_table_schema = Products_Table_Schema(many=True)

order_table_schema = Orders_Table_Schema()
orders_table_schema = Orders_Table_Schema(many=True)

@app.route("/findoneorder/",methods=["GET"])
def findoneorder():
    try:
        id_arg = request.args.get('id')
        one_order = Orders_Table.query.get(id_arg)

        if id_validate(id_arg):
            one_order = Orders_Table.query.get(id_arg)

            return whatcode(200, one_order.status_order)
        else:
            return whatcode(400)
    except Exception:
        return whatcode(500)


#Leer todos las ordenes
@app.route('/findallorders', methods=['GET'])
def findallorders():
    try:
        allorders = Orders_Table.query.all()
        result = orders_table_schema.dump(allorders)

        return whatcode(200, result)
    except Exception:
        return whatcode(500)

#Actualizar Estado del pedido
@app.route('/updateorder/', methods=['PUT'])
def update_order():
    try:
        id_arg = request.args.get('id')
        if id_validate(id_arg):
            update_order = Orders_Table.query.get(id_arg)

            data = request.get_json(force=True)
            status_order = data['status_order']
            if status_order_validate(status_order):
                update_order.status_order, update_order.check_preparation = status_order_validate(status_order)
            else:
                return whatcode(400)
        else:
            return 

        db.session.commit()
        return whatcode(200, status_order)
    
    except Exception:
        return whatcode(500)

#Crear Pedido
@app.route("/createorder", methods=['POST'])
def insert_order():
    try:
        data = request.get_json(force=True)
        client_name = data['client_name']
        products_name = data['products_name']
        client_addrs = data['client_addrs']
        client_city = data['client_city']
        client_phone = data['client_phone']

        client_info = [client_name, client_addrs,
                       client_city, client_phone]
        if create_order_validate(client_info):
            client_info = create_order_validate(client_info)
            order_date = today()
            total_count = 0
            if products_name.count(",") >= 1:
                products_names_splitted = products_name.split(",")
                if create_order_validate(products_names_splitted):
                    no_product = len(products_names_splitted)
                    for product in products_names_splitted:
                        product = product.strip()
                        find_product = Products_Table.query.filter(Products_Table.product_name == product).one()
                        if find_product.product_price:
                            total_count += int(find_product.product_price)
                else:
                    return whatcode(400)
            elif products_name:
                no_product = 1
                product = Products_Table.query.filter(Products_Table.product_name == products_name).one()
                total_count = product.product_price
            else:
                return whatcode(400)
        else:
            return whatcode(400)

        status_order = "EN ESPERA"
        check_preparation = ""
        check_on_route = ""
        check_delivered = ""
        
        query = [client_info[0], order_date, products_name, 
                 no_product, total_count, client_info[1],
                 client_info[2], client_info[3], status_order,
                 check_preparation, check_on_route, check_delivered]
        
        new_order = Orders_Table(query)
        db.session.add(new_order)
        db.session.commit()

        return whatcode(200, [new_order.order_id, new_order.status_order])
    except Exception:
        return whatcode(500)

#Crear articulo (Comida)
@app.route("/createfood", methods=['POST'])
def insert_food():
    try:
        data = request.get_json(force=True)
        product_name = data["product_name"]
        product_price = data["product_price"]

        new_food = Products_Table(product_name, product_price)
        db.session.add(new_food)
        db.session.commit()
        
        if create_order_validate([product_name, product_price]):
            new_food = Products_Table(product_name, product_price)
            db.session.add(new_food)
            db.session.commit()
            return whatcode(200, [new_food.product_name, new_food.product_price])
        else:
            return whatcode(400)
    except Exception:
        return whatcode(500)
@app.route('/', methods=['GET'])
def home():
    return whatcode("Welcome")


if __name__ == '__main__':
    app.run(port=5000, debug=True)